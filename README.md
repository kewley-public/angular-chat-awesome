# Chat

Angular chat module using [ngx-quill](https://www.npmjs.com/package/ngx-quill) for text input.

## Demo

Demo UI is hosted on AWS CloudFront which can be found [here](http://d2mvk4oe8oeoo7.cloudfront.net)

## TODO Items
1. Mobile responsive
2. Remove Karma/Protractor configs from project

## NPM Library Source code

Can be found [here](https://gitlab.com/kewley-public/angular-chat-awesome/tree/master/projects/angular-chat-awesome)

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run the command `npm run build:library` to build the library and `npm run build`
to build the UI.

## Running tests

Run the command `npm run test:ci`. This will use our testing framework
[Jest](https://jestjs.io/) to run all of the tests.
