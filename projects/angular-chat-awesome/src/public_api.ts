/*
 * Public API Surface of angular-chat-awesome
 */
export * from './lib/angular-chat-awesome.component';
export * from './lib/angular-chat-awesome.module';
export * from './lib/models/chat-message.model';
export * from './lib/models/chat-message-type.model';

