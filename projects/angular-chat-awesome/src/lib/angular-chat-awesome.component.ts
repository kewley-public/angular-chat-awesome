import {
  AfterViewChecked,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ChatMessage } from './models/chat-message.model';
import { ChatMessageType } from './models/chat-message-type.model';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'lib-angular-chat-awesome',
  templateUrl: './angular-chat-awesome.component.html',
  styleUrls: ['./styles.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AngularChatAwesomeComponent implements OnChanges, AfterViewChecked {

  @ViewChild('messagesContainer') private messagesContainer: ElementRef;
  @Input() contactName: string;
  @Input() messages: ChatMessage[] = [];
  @Input() timezone = 'UTC';
  @Input() maxHeight = '100%';
  @Input() height = '100%';

  @Input() newMessage: string;
  @Output() newMessageChange = new EventEmitter();

  public toolbar = [
    ['bold', 'italic', 'underline', 'strike'],
    ['blockquote'],
    [{'header': [1, 2, 3, 4, 5, 6, false]}],
    [{'list': 'ordered'}, {'list': 'bullet'}],
    [{'script': 'sub'}, {'script': 'super'}],
    [{'indent': '-1'}, {'indent': '+1'}],
    ['link', 'image'],
  ];
  public form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      'message': [],
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['newMessage'] && !changes['newMessage'].currentValue) {
      this.form.patchValue({'message': null});
    }
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  determineMessageClass(message: ChatMessage) {
    switch (message.type) {
      case ChatMessageType.SENT:
        return 'sent';
      case ChatMessageType.RECEIVED:
        return 'received';
      default:
        return '';
    }
  }

  onContentChange(event: any) {
    this.newMessage = event.html;
    this.newMessageChange.emit(this.newMessage);
  }

  determineFlexClass(message) {
    switch (message.type) {
      case ChatMessageType.SENT:
        return 'justify-content-start';
      case ChatMessageType.RECEIVED:
        return 'flex-row-reverse';
      default:
        return '';
    }
  }

  private scrollToBottom(): void {
    try {
      this.messagesContainer.nativeElement.scrollTop = this.messagesContainer.nativeElement.scrollHeight;
    } catch (err) {
      console.error(err);
    }
  }

}
