import { NgModule } from '@angular/core';
import { AngularChatAwesomeComponent } from './angular-chat-awesome.component';
import { CommonModule } from '@angular/common';
import { QuillModule } from 'ngx-quill';
import { DateTimeFromMillisPipe } from './pipes/pipes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    QuillModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AngularChatAwesomeComponent,
    DateTimeFromMillisPipe,
  ],
  exports: [AngularChatAwesomeComponent],
})
export class AngularChatAwesomeModule {
}
